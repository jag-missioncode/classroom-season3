/**
 * <a class="menu-item" href="">Home </a>
    <a class="menu-item" href="">Assignments</a>
    <a class="menu-item" href="">Notes</a>
    <a class="menu-item" href="">Checklist</a>
    <a class="menu-item" href="https://google.com/support">Support</a>
 */
const navitems = [{
    text : "Home",
    link : "home.html",
    class : "menu-item",
    isActive : true,
    add : function(){

    }
},
{
    text : "Assignments",
    link : "assignments.html",
    class : "menu-item",
    isActive : false
},
{
    text : "Notes",
    link : "notes.html",
    class : "menu-item",
    isActive : false
},
{
    text : "Checklist",
    link : "checklist.html",
    class : "menu-item",
    isActive : false
},
{
    text : "Tools",
    link : "tools.html",
    class : "menu-item",
    isActive : false
},
{
    text : "Support",
    link : "mailto:jagadesh@missioncode.org",
    class : "menu-item",
    isActive : false
}];

const menuElement = document.getElementsByClassName("menu")[0];
/* console.log('Before Adding dynamically');
console.log(menuElement); */
let menuItemElement = "";
navitems.forEach(function(navItem){

    // <a class="menu-item" href="">Home</a>
    menuItemElement = menuItemElement + '<a class="'+ navItem.class +'" href="'+navItem.link+'">' + navItem.text + '</a>';
    
});
// console.log("Nav Items", menuItemElement);
menuElement.innerHTML = menuItemElement;
