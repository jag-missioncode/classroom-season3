const assignments = [
    {
        imageSrc: "https://placeimg.com/140/80/any",
        title: "Html Tags by category",
        desc: "Inline elements vs Block Elements",
        createdOn: "Sep 8th 2019"
    },
    {
        imageSrc: "https://placeimg.com/140/80/any",
        title: "Get Started",
        desc: "Button in html / css",
        createdOn: "Sep 10th 2019"
    }
];

function generateCards() {

    const cardElements = assignments.map(function (assignment) {
        return `<div class="card">
    
                    <span class="icon">
                        <i class="fa fa-heart"></i>
                    </span>
                    <img src="${assignment.imageSrc}" alt="HTML by tags assignment" />
                    <p class="title">${assignment.title}</p>
                    <p class="title">${assignment.desc}</p>
                    <p>Created on : ${assignment.createdOn}</p>
    
                    <footer>
                        <a class="button button-orange">UpVote</a>
                        <a class="button button-blue" href="../assingments/html-tags-by-category.html">View</a>
                    </footer>
    
                </div>`

    });

    const cardsElement = document.querySelector(".cards");
    cardsElement.innerHTML = cardElements.join('');
}

function createAssignmentCard(event) {
    event.preventDefault();
    const inputElements = document.querySelectorAll('.assignment-form input');

    const newAssignment = {
        imageSrc: "https://placeimg.com/140/80/any",
        title: inputElements[0].value,
        desc: inputElements[1].value,
        createdOn: inputElements[2].value
    };

    assignments.push(newAssignment);
    generateCards();
}

generateCards();