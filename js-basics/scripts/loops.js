const schedule = ["Jan 11 2018","Feb 11 2018","Mar 11 2018","Apr 11 2018","May 11 2018","Jun 11 2018","Jul 11 2018","Aug 11 2018","Sep 11 2018","Oct 11 2018","Nov 11, 2018","Dec 11, 2018"];

const mixedDatas = ["Jan 11 2018", 10.77, true, 'S'];

console.log("Total no of schedule : ", schedule.length);
/**
 * First section " let index = 0" - initialization part
 * Second section " index < array.length " - Decision part
 * Third section " index++ " - incremental part
 */
let weekends = [];
let index = 0;
for (; index < schedule.length;) {

    const currentSchedule = schedule[index];
    const dateObj = new Date(currentSchedule);
    const isHoliday = isWeekend(dateObj);

    if(isHoliday === true){
       weekends.push(currentSchedule);
       schedule.splice(index, 1);
    }else{
        index = index + 1;
    }
    
}

console.log("Total holiday in this schedule is : ", weekends, weekends.length);
console.log("Total workday in this schedule is : ", schedule, schedule.length);