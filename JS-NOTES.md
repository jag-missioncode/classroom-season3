Data Type
--------
Dynamic data typing 

var

let , const

string, char, int, float, boolean, array, object


Basic Expressions
-----------------
- (+) Addition or concatination 
- (-) Substraction
- (*) Multiplication
- (/) Division
- (%) Modulus division, also called remainder division

Refer math class for more expression

Decisions
----------
Relational Operators

< less than
<= less than equal
> greater than
>= greater than eqal
== equal , === strict equal
!== not equal, !=== strict not equal

if else, else if, switch

Logical Operators
------
&& and
|| or


Loops ( Repeat block of code )
------------------------------
for, while, for in
