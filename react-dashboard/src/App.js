import React from 'react';
import Home from './pages/home/Home';
import Nav from './shared/navigation/Nav';
import Footer from './shared/footer/Footer';
import Assignments from './pages/assignments/Assignments';
import Checklist from './pages/checklist/Checklist';

import {
  BrowserRouter,
  Switch,
  Route
} from 'react-router-dom';

function Main() {
  return (
    <BrowserRouter>
      <main>
        <Nav />

        <Switch>
          
          <Route path="/home">
            <Home />
          </Route>
          <Route path="/assignments">
            <Assignments />
          </Route>
          <Route path="/checklist">
            <Checklist />
          </Route>
          <Route path="/">
            <Assignments />
          </Route>
      
        </Switch>

        <Footer />

      </main>
    </BrowserRouter>
  );
}

export default Main;
