import React from 'react';
import Announcement from './Announcement';
import Countdown from './Countdown';

function Banner(){
    return (
        <div className="jumbotron jumbotron-fluid">
        <div className="container">

            <div className="wrapper bg-dark p-5">
                {/* <h1 className="display-4 text-light">Assignment Collections</h1>
                <p className="lead text-white-50">Explore assignments by week or by language
                </p>
                <button type="button" className="btn btn-primary btn-lg mr-3">Explore Assingments by week</button>
                <button type="button" className="btn btn-light btn-lg">Explore Assignments by language</button> */}
                <Announcement styleClass="display-4 text-white text-center">
                    There are only <Countdown days="16" hours="1" minutes="37" seconds="59"/> until final presentation
                </Announcement>
            </div>
            
        </div>
    </div>
    );
}

export default Banner;