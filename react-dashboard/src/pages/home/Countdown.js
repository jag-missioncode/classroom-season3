import React from 'react';

function Countdown(props) {
    /* const [seconds, setSeconds] = React.useState(props.seconds);
    const [minutes, setMinutes] = React.useState(props.minutes); */
    const defaultState = {
        days: props.days,
        hours: props.hours,
        minutes: props.minutes,
        seconds: props.seconds
    };
    // Array destructing
    const [countdown, setCountdown] = React.useState(defaultState);

    // Arrow function ()=>{}
    setTimeout(() => {
        let newSeconds = countdown.seconds - 1;
        let newMinutes = countdown.minutes;
        if (newSeconds === 0) {
            newMinutes = countdown.minutes - 1;
            newSeconds = 59;
        }
        setCountdown({
            days: countdown.days,
            hours: countdown.days,
            minutes: newMinutes,
            seconds: newSeconds
        });

    }, 1000);

    return (
        <p>
            <span className="badge badge-warning">{
                countdown.days
            }</span> days
            <span className="badge badge-warning">{
                countdown.hours
            }</span> hours
            <span className="badge badge-warning">
                {
                    countdown.minutes
                }
            </span> minutes
            <span className="badge badge-warning">
                {
                    countdown.seconds
                }
            </span> seconds
        </p>
    )
}

export default Countdown;