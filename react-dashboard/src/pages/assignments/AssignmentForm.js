import React from 'react';

function AssignmentForm(props) {
    const [assignmentFormVal, setAssignmentFormVal]= React.useState(props.record);
    return (
        <div className="card border-secondary mb-3">
            <div className="card-header">New assignment Form</div>
            <div className="card-body text-secondary">
                <form onSubmit={handleFormSubmit}>
                    <div className="form-group">
                        <label htmlFor="titleInput">Title</label>
                        <input type="text" required value={assignmentFormVal.title} onChange={handleTitleChange} className="form-control" id="titleInput" placeholder="Enter Assignment title" />
                    </div>
                    <div className="form-group">
                        <label htmlFor="titleDesc">Description</label>
                        <input type="text" required className="form-control" id="titleDesc" placeholder="Enter Assignment description" value={assignmentFormVal.desc} onChange={handleDescChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="titleDate">Created Date</label>
                        <input type="date" required className="form-control" id="titleDate" placeholder="MM/DD/YYYY" value={assignmentFormVal.createdOn} onChange={handleDateChange} />
                    </div>
                    <button type="submit" className="btn btn-primary">Add assignment</button>
                </form>
            </div>
        </div>
    );

    function handleFormSubmit($event){
        $event.preventDefault();
        console.log("handle form submit", assignmentFormVal);
        props.recordChange(assignmentFormVal);
        setAssignmentFormVal(props.record);
    }

    function handleTitleChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: $event.target.value,
            desc: assignmentFormVal.desc,
            createdOn: assignmentFormVal.createdOn
        })
    }

    function handleDescChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: $event.target.value,
            createdOn: assignmentFormVal.createdOn
        })
    }
    function handleDateChange($event){
        setAssignmentFormVal({
            imageSrc: assignmentFormVal.imageSrc,
            title: assignmentFormVal.title,
            desc: assignmentFormVal.desc,
            createdOn: $event.target.value
        })
    }
}

export default AssignmentForm;